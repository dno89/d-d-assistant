require "math"

-- ranged autilities
-- default values
default_ranged = {NAME = "Unarmed", TPC = 0, DAN = 0, DAD = "-", CRI = "", INC = "3m"}

-- native support for magic weapons
function default_ranged:magic(mag)
	local res = {}
	setmetatable(res, getmetatable(self))
	-- setmetatable(res, self)
	for k, v in pairs(self) do
		res[k] = v
	end
	res.TPC = res.TPC + mag	
	res.DAN = res.DAN + mag	
	res.NAME = res.NAME .. " +" .. mag
	return res
end

local ranged_mt = {__index = default_ranged, __add = default_ranged.magic}

function ranged(spec)
	setmetatable(spec, ranged_mt)
	return spec
end

-- ranged
dofile("database/ranged.lua")
