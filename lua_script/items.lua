-- items utilities

-- accumulate items characteristics
function item_modifiers(items, char)
	-- accumulate various values
	local accum = 0

	for k, v in pairs(items) do
		if k > 0 and v[char] then
			accum = accum + v[char]
		end
	end
	
	return accum
end


-- item list
dofile("database/items.lua")
