-- il livello del NPC
LIV = d(4, 1)

-- il dado vita
DV = 8

-- le caratteristiche
FOR = 10 -- best_d(6, 4, 3)
DES = best_d(6, 4, 3)
COS = best_d(6, 4, 3)
INT = d(6, 3)
SAG = d(6, 3)
CAR = d(6, 3)

-- il tipo di bab: buono, medio , scarso
BAB = buono

-- velocita
VEL = 6

-- il tipo di tiri salvezza: alto, basso
TEM = alto
RIF = basso
VOL = basso

-- talenti
TAL = {iniziativa_migliorata, mischia_focalizzata, distanza_specializzata}

-- armatura
ARM = corazza_di_bande

-- scudo
SCU = scudo_leggero+2

-- armi da mischia
A_MIS = spada_corta+1

-- armi da distanza
A_DIS = rand_choice(arco_lungo, arco_corto+2, arco_lungo+10)
-- A_DIS = arco_lungo+1

-- monete
MO = d(4, 1)
MA = d(8, 2)
MR = d(6, 1)

-- oggetti
OGG = {guanti_forza_2}

-- attacco
ATT = {"attacco furtivo +1d6", "disarmare +4"}

-- difesa
DIF = {"schivare", "furia orchesca"}
