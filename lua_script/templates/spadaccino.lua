-- il livello del NPC
LIV = d(4, 3)

-- il dado vita
DV = 8

-- le caratteristiche
FOR = best_d(6, 6, 3)
DES = best_d(6, 4, 3)
COS = best_d(6, 4, 3)
INT = d(6, 3)
SAG = d(6, 3)
CAR = d(6, 3)

-- il tipo di bab: buono, medio , scarso
BAB = buono

-- il tipo di tiri salvezza: alto, basso
TEM = alto
RIF = basso
VOL = basso

-- talenti
TAL = {mischia_focalizzata, mischia_specializzata}

-- armatura
ARM = corazza_di_bande+2

-- scudo
SCU = scudo_leggero+2

-- armi da mischia
A_MIS = spada_lunga+1

-- armi da distanza
