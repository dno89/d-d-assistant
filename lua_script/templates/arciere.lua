-- il livello del NPC
LIV = d(4, 2)

-- il dado vita
DV = 8

-- le caratteristiche
FOR = best_d(6, 4, 3)
DES = best_d(6, 5, 3)
COS = best_d(6, 4, 3)
INT = d(6, 3)
SAG = d(6, 3)
CAR = d(6, 3)

-- il tipo di bab: buono, medio , scarso
BAB = buono

-- il tipo di tiri salvezza: alto, basso
TEM = alto
RIF = alto
VOL = basso

-- talenti
TAL = {iniziativa_migliorata, distanza_focalizzata, distanza_specializzata}

-- armatura

-- scudo

-- armi da mischia

-- armi da distanza
A_DIS = arco_lungo+3
