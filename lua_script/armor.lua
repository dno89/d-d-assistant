require "math"

-- armors utilities
-- default values
default_armor = {NAME = "No armor", BONUS = 0, DES_MAX = math.huge, PEN = 0}
-- native support for magic armor
function default_armor:magic(mag)
	local res = {}
	setmetatable(res, getmetatable(self))
	for k, v in pairs(self) do                                                                                         
		res[k] = v
	end
	res.BONUS = res.BONUS + mag	
	res.NAME = res.NAME .. " +" .. mag
	return res
end

local armor_mt = {__index = default_armor, __add = default_armor.magic}
function armor(spec)
	setmetatable(spec, armor_mt)
	return spec
end

-- armor list
dofile("database/armor.lua")
