-- lista armi da distanza
arco_corto = ranged{NAME = "Arco corto", TPC = 0, DAN = 0, DAD = "1d6", CRI = "x3", INC = "18m"}
arco_lungo = ranged{NAME = "Arco lungo", TPC = 0, DAN = 0, DAD = "1d8", CRI = "x3", INC = "30m"}
