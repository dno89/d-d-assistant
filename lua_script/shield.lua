require "math"

-- shield utilities
-- default values
default_shield = {NAME = "No shield", BONUS = 0, DES_MAX = math.huge, PEN = 0}
-- native support for magic armor
function default_shield:magic(mag)
	local res = {}
	setmetatable(res, getmetatable(self))
	for k, v in pairs(self) do                                                                                         
		res[k] = v                                                                                                     
	end
	res.BONUS = res.BONUS + mag	
	res.NAME = res.NAME .. " +" .. mag
	return res
end

local shield_mt = {__index = default_shield, __add = default_shield.magic}
function shield(spec)
	setmetatable(spec, shield_mt)
	return spec
end

-- shields
dofile("database/shield.lua")
