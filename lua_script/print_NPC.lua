require "io"
require "string"

print("\t\t>> NPC printing utility <<")

local title = "------ LIVELLO / DADO VITA / TAGLIA"
print("LIV: ", NPC.LIV)
print("TAGLIA:", NPC.TAG.NAME)
print("DV: ", "d"..NPC.DV)
print(string.rep("-", #title))

title = "------ CARATTERISTICHE"
print(title)
print("FOR: " .. NPC.FOR, " | " .. NPC.FOR_mod)
print("DES: " .. NPC.DES, " | " .. NPC.DES_mod)
print("COS: " .. NPC.COS, " | " .. NPC.COS_mod)
print("INT: " .. NPC.INT, " | " .. NPC.INT_mod)
print("SAG: " .. NPC.SAG, " | " .. NPC.SAG_mod)
print("CAR: " .. NPC.CAR, " | " .. NPC.CAR_mod)
print(string.rep("-", #title))

title = "------ PF / INIZIATIVA / BAB / CA"
print(title)
print("PF: ", NPC.PF)
print("CA: ", NPC.CA)
print("INIZ: ", NPC.INIZ, "(" .. NPC.INIZ_val .. ")")
print("BAB: ", NPC.BAB_val)
print(string.rep("-", #title))

title = "------ TIRI SALVEZZA"
print(title)
print("TEM: ", NPC.TEM_val)
print("RIF: ", NPC.RIF_val)
print("VOL: ", NPC.VOL_val)
print(string.rep("-", #title))

title = "------ MANOVRE IN COMBATTIMENTO"
print(title)
print("BMC: ", NPC.BMC)
print("DMC: ", NPC.DMC)
print(string.rep("-", #title))

title =  "------ MISCHIA"
print(title)
print(NPC.A_MIS.NAME .. ":", NPC.MIS.TPC .. " (" .. NPC.MIS.DAN .. ")")
print(string.rep("-", #title))

title =  "------ DISTANZA"
print(title)
print(NPC.A_DIS.NAME .. ":", NPC.DIS.TPC .. " (" .. NPC.DIS.DAN .. ")")
print(string.rep("-", #title))

title = "------ ARMATURA E SCUDO"
print(title)
print(NPC.ARM.NAME .. "(Bonus: " .. NPC.ARM.BONUS, "DES max: " .. NPC.ARM.DES_MAX, "Pen: " .. NPC.ARM.PEN .. ")")
print(NPC.SCU.NAME .. "(Bonus: " .. NPC.SCU.BONUS, "DES max: " .. NPC.SCU.DES_MAX, "Pen: " .. NPC.SCU.PEN .. ")")
print(string.rep("-", #title))

title = "------ MONETE"
print(title)
print("MP:", NPC.MON.MP)
print("MO:", NPC.MON.MO)
print("MA:", NPC.MON.MA)
print("MR:", NPC.MON.MR)
print(string.rep("-", #title))

title = "------ TALENTI (" .. NPC.TAL[0] .. ")"
print(title)
for k, v in pairs(NPC.TAL) do
	if k > 0 then
		print("> " .. v.NAME)
	end
end
print(string.rep("-", #title))

title = "------ OGGETTI (" .. NPC.OGG[0] .. ")"
print(title)
for k, v in pairs(NPC.OGG) do
	if k > 0 then
		print("- " .. v.NAME)
	end
end
print(string.rep("-", #title))
