-- feats utilities

-- accumulate feats characteristics
function feat_modifiers(feats, char)
	-- accumulate various values
	local accum = 0

	for k, v in pairs(feats) do
		if k > 0 and v[char] then
			accum = accum + v[char]
		end
	end
	
	return accum
end


-- feats list
dofile("database/feats.lua")
