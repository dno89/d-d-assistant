require "math"

-- melee utilities
-- default values
default_melee = {NAME = "Unarmed", TPC = 0, DAN = 0, DAD = "1d4", CRI = "x2"}

-- native support for magic weapons
function default_melee:magic(mag)
	local res = {}
	setmetatable(res, getmetatable(self))
	for k, v in pairs(self) do
		res[k] = v
	end
	res.TPC = res.TPC + mag	
	res.DAN = res.DAN + mag	
	res.NAME = res.NAME .. " +" .. mag
	return res
end

local melee_mt = {__index = default_melee, __add = default_melee.magic}

function melee(spec)
	setmetatable(spec, melee_mt)
	return spec
end

-- melee weapons
dofile("database/melee.lua")
