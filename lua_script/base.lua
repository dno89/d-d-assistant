require "math"
require "os"

-- create global functions

-- call randomization function
math.randomseed(os.time())

--- SUPPORT FUNCTIONS

-- obtain a modifier from a characteristic
function modifier(x) 
	return math.floor((x-10)/2)
end

-- dice utilities
dofile("dice.lua")

-- choice among argument at random
function rand_choice(...)
	local nargs = select('#', ...)
	local index = d(nargs, 1)
	return select(index, ...)
end

-- type of bab
function buono(liv)
	return liv
end
function medio(liv)
	return math.floor(liv * 3 / 4)
end
function scarso(liv)
	return math.floor(liv / 2)
end

-- type of ST
function alto(liv)
	return 2 + math.floor(liv/2)
end
function basso(liv)
	return math.floor(liv/3)
end

-- feats utilities
dofile("feats.lua")

-- taglie
dofile("size.lua")

-- armor
dofile("armor.lua")

-- shield
dofile("shield.lua")

-- melee weapons
dofile("melee.lua")

-- ranged weapons
dofile("ranged.lua")

-- items
dofile("items.lua")

-- the default NPC table
function default_npc()
	local npc = {}
	-- physics / mental
	npc.FOR = 10
	npc.DES = 10
	npc.COS = 10
	npc.INT = 10
	npc.SAG = 10
	npc.CAR = 10
	-- lvl
	npc.LIV = 1
	-- dv
	npc.DV = 4
	-- bab
	npc.BAB = scarso
	-- ts
	npc.TEM = basso
	npc.RIF = basso
	npc.VOL = basso
	-- talenti
	npc.TAL = {}
	-- taglia
	npc.TAG = media
	-- armatura
	npc.ARM = default_armor
	-- scudo
	npc.SCU = default_shield
	-- mischia
	npc.A_MIS = default_melee
	-- distanza
	npc.A_DIS = default_ranged
	-- icon
	npc.ico = ":new/icons/res/fighter.png"
	-- money
	npc.MON = {MP = 0, MO = 0, MA = 0, MR = 0}
	-- items
	npc.OGG = {}
	-- velocita
	npc.VEL = 9
	-- difesa
	npc.DIF = {}
	-- attacco
	npc.ATT = {}

	return npc
end
