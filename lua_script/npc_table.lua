-- create the default NPC table
NPC = default_npc()

-- oggetti
NPC.OGG = OGG or NPC.OGG
NPC.OGG[0] = table.maxn(NPC.OGG)
OGG = nil

-- caratteristiche fisiche/psichiche
NPC.FOR = (FOR or NPC.FOR) + item_modifiers(NPC.OGG, "FOR")
NPC.FOR_mod = modifier(NPC.FOR)
FOR = nil

NPC.DES = (DES or NPC.DES) + item_modifiers(NPC.OGG, "DES")
NPC.DES_mod = modifier(NPC.DES)
DES = nil

NPC.COS = (COS or NPC.COS) + item_modifiers(NPC.OGG, "COS")
NPC.COS_mod = modifier(NPC.COS)
COS = nil

NPC.INT = (INT or NPC.INT) + item_modifiers(NPC.OGG, "INT")
NPC.INT_mod = modifier(NPC.INT)
INT = nil

NPC.SAG = (SAG or NPC.SAG + item_modifiers(NPC.OGG, "SAG"))
NPC.SAG_mod = modifier(NPC.SAG)
SAG = nil

NPC.CAR = (CAR or NPC.CAR) + item_modifiers(NPC.OGG, "CAR")
NPC.CAR_mod = modifier(NPC.CAR)
CAR = nil

-- livello
NPC.LIV = LIV or NPC.LIV
LIV = nil

-- bonus attacco base
NPC.BAB = BAB or NPC.BAB
NPC.BAB_val = NPC.BAB(NPC.LIV)
BAB = nil

-- dado vita
NPC.DV = DV or NPC.DV
DV = nil

-- punti ferita
NPC.PF = NPC.DV + d(NPC.DV, NPC.LIV-1) + NPC.LIV*NPC.COS_mod

-- armatura
NPC.ARM = ARM or NPC.ARM
ARM = nil

-- scudo
NPC.SCU = SCU or NPC.SCU
SCU = nil

-- forza il bonux max di DES
NPC.DES_mod = math.min(NPC.DES_mod, NPC.ARM.DES_MAX, NPC.SCU.DES_MAX)

-- tiri salvezza
NPC.TEM = TEM or NPC.TEM
NPC.TEM_val = NPC.TEM(NPC.LIV) + NPC.COS_mod
TEM = nil

NPC.RIF = RIF or NPC.RIF
NPC.RIF_val = NPC.RIF(NPC.LIV) + NPC.DES_mod
RIF = nil

NPC.VOL = VOL or NPC.VOL
NPC.VOL_val = NPC.VOL(NPC.LIV) + NPC.SAG_mod
VOL = nil

-- taglia
NPC.TAG = TAG or NPC.TAG
TAG = nil

-- manovre in combattimento
NPC.BMC = NPC.BAB_val + NPC.FOR_mod + NPC.TAG.BMC
NPC.DMC = NPC.BAB_val + NPC.FOR_mod + NPC.DES_mod + NPC.TAG.DMC

-- talenti
NPC.TAL = TAL or NPC.TAL
NPC.TAL[0] = table.maxn(NPC.TAL)
TAL = nil

-- iniziativa
NPC.INIZ = NPC.DES_mod + feat_modifiers(NPC.TAL, "INIZ")
NPC.INIZ_val = NPC.INIZ + d(20, 1)

-- calcolo CA
NPC.CA = 10 + NPC.ARM.BONUS + NPC.SCU.BONUS + NPC.DES_mod + NPC.TAG.CA
NPC.CA_cont = 10 + NPC.DES_mod + NPC.TAG.CA
NPC.CA_sprovv = 10 + NPC.ARM.BONUS + NPC.SCU.BONUS + NPC.TAG.CA

-- velocita'
NPC.VEL = VEL or NPC.VEL
VEL = nil

-- difesa
NPC.DIF = DIF or NPC.DIF
NPC.DIF[0] = table.maxn(NPC.DIF)
DIF = nil

-- attacco
NPC.ATT = ATT or NPC.ATT
NPC.ATT[0] = table.maxn(NPC.ATT)
ATT = nil

-- arma da mischia
NPC.A_MIS = A_MIS or NPC.A_MIS
A_MIS = nil

-- arma da distanza
NPC.A_DIS = A_DIS or NPC.A_DIS
A_DIS = nil

-- mischia
NPC.MIS = {}
NPC.MIS.TPC = NPC.BAB_val + NPC.TAG.TPC + NPC.FOR_mod + NPC.A_MIS.TPC + feat_modifiers(NPC.TAL, "MIS_TPC")
NPC.MIS.DAN = NPC.A_MIS.DAD .. " + " .. (NPC.FOR_mod + NPC.A_MIS.DAN + feat_modifiers(NPC.TAL, "MIS_DAN"))

-- distanza
NPC.DIS = {}
NPC.DIS.TPC = NPC.BAB_val + NPC.TAG.TPC + NPC.DES_mod + NPC.A_DIS.TPC + feat_modifiers(NPC.TAL, "DIS_TPC")
NPC.DIS.DAN = NPC.A_DIS.DAD .. " + " .. (NPC.A_DIS.DAN + feat_modifiers(NPC.TAL, "DIS_DAN"))

-- the icon
NPC.ico = ico or NPC.ico
ico = nil

-- monete
NPC.MON.MP = MP or NPC.MON.MP
NPC.MON.MO = MO or NPC.MON.MO
NPC.MON.MA = MA or NPC.MON.MA
NPC.MON.MR = MR or NPC.MON.MR
MP = nil
MO = nil
MA = nil
MR = nil

