#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidget>
#include <QListView>
#include <QMenu>

////std libs
#include <vector>
//#include <list>
#include <functional>
#include <algorithm>
#include <cassert>
#include <iostream>
////Qt
#include <QDir>
///lua
#include <lua.hpp>
///other forms
#include "npcinsert.h"
#include "pcinsert.h"

namespace Ui {
class MainWindow;
}

///support objects
typedef std::pair<QString, std::string> named_file_type;

//virtual base class
struct character {
    //nome
    std::string NAME;

    //valore d'iniziativa
    int INIZ_val;

    //the default icon
    std::string ico = ":/new/icons/res/fighter.png";

    virtual ~character() {};
};

///generic NPC struct
struct npc : public character {
    //punti ferita
    int PF, remaining_PF;

    virtual ~npc() {};
};

///template npc struct
struct defense {
    std::string NAME;
    //bonus armatura/scudo
    int BONUS;
    //max bonus destrezza
    int DES_MAX;
    //penalità alla prova
    int PEN;
};

struct offense {
    std::string NAME;
    //bonus tiro per colpire
    int TPC;
    //bonus al danno
    int DAN;
    //dado del danno
    std::string DAD;
    //critico
    std::string CRI;
};

struct money {
    int MP, MO, MA, MR;
};

struct attack {
    int TPC;
    std::string DAN;
};

struct template_npc : public npc {
    //carattetistiche
    int FOR, FOR_mod;
    int DES, DES_mod;
    int COS, COS_mod;
    int INT, INT_mod;
    int SAG, SAG_mod;
    int CAR, CAR_mod;

    //livello
    int LIV;

    //bonus attacco base
    int BAB;

    //dado vita
    int DV;

    //armatura e scudo
    defense ARM, SCU;

    //tiri salvezza
    int TEM, RIF, VOL;

    //taglia
    std::string TAG;

    //manovre in combattimento
    int BMC, DMC;

    //talenti
    std::vector<std::string> TAL;

    //bonus iniziativa
    int INIZ;

    //classe armatura
    int CA, CA_cont, CA_sprovv;

    //armi mischia e distanza
    offense A_MIS, A_DIS;

    //attacchi in mischia e distanza
    attack MIS, DIS;

    //soldi
    money MON;

    //oggetti vari
    std::vector<std::string> OGG;

    //abilita di difesa
    std::vector<std::string> DIF;
    std::vector<std::string> ATT;

    //la velocita base
    int VEL;

    //icon to display
//    std::string ico;
};

//model for model/view paradigm
class InitiativeListModel : public QAbstractListModel {
public:
    InitiativeListModel(std::map<std::string, npc>& npc_table, std::map<std::string, template_npc>& template_npc_table, std::map<std::string, character>& pc_table) : m_template_table(template_npc_table), m_npc_table(npc_table), m_pc_table(pc_table)
    {}

    //virtual functions
    int rowCount(const QModelIndex &parent = QModelIndex()) const {
        return m_initiative_list.size();
    }
    int columnCount(const QModelIndex &parent = QModelIndex()) const {
        return 1;
    }
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const {
        assert(index.column() == 0);

        if(role == Qt::DisplayRole) {
            QString name = QString(m_initiative_list[index.row()].c_str());
            int iniz = GetInitiative(m_initiative_list[index.row()]);
            name += QString(" (%1)").arg(iniz);
            //std::cerr << name.toLocal8Bit().data() << std::endl;
            return name;
        } else if(role == Qt::DecorationRole) {
            std::string name = m_initiative_list[index.row()];
            if(m_template_table.count(name)) {
                name = m_template_table.at(name).ico;
            } else if(m_npc_table.count(name)) {
                name = m_npc_table.at(name).ico;
            } else {
                name = m_pc_table.at(name).ico;
            }
            //name is the icon
            return QIcon(QString(name.c_str()));
        }

        return QVariant();
    }
    void AddInitiativeEntry(const std::string& name) {
        m_initiative_list.push_back(name);
        Sort();
        //notify data change
        emit dataChanged(createIndex(0, 0), createIndex(m_initiative_list.size()-1, 0));
    }
    std::string GetUName(int index) const {
        if( index >= 0 && index < m_initiative_list.size()) {
            return m_initiative_list[index];
        }

        return "";
    }
    bool Swap(int i1, int i2) {
        if(i1 >= 0 && i2 >= 0 && i1 < m_initiative_list.size() && i2 < m_initiative_list.size()) {
            int new_initiative = findInitiative(i2);

            findInitiative(i1) = new_initiative;

            std::swap(m_initiative_list[i1], m_initiative_list[i2]);
            emit dataChanged(createIndex(0, 0), createIndex(m_initiative_list.size()-1, 0));
            return true;
        }

        return false;
    }
    bool Remove(int index) {
        if(index >= 0 && index < m_initiative_list.size()) {
            m_initiative_list.erase(m_initiative_list.begin()+index);
            emit dataChanged(createIndex(0, 0), createIndex(m_initiative_list.size()-1, 0));
            return true;
        }

        return false;
    }
    int& findInitiative(int index) {
        const std::string uname = m_initiative_list[index];

        if(m_template_table.count(uname)) return m_template_table[uname].INIZ_val;
        if(m_npc_table.count(uname)) return m_npc_table[uname].INIZ_val;
        if(m_pc_table.count(uname)) return m_pc_table[uname].INIZ_val;
    }


private:
    //actual data
    std::vector<std::string> m_initiative_list;
    //the lookup tables
    std::map<std::string, template_npc>& m_template_table;
    std::map<std::string, npc>& m_npc_table;
    std::map<std::string, character>& m_pc_table;
    //the comparator
    bool Comp(const std::string& s1, const std::string& s2) {
        int i1 = GetInitiative(s1), i2 = GetInitiative(s2);

        return i1 > i2;
    }

    //functions
    void Sort() {
        std::stable_sort(m_initiative_list.begin(), m_initiative_list.end(), std::bind(&InitiativeListModel::Comp, this, std::placeholders::_1, std::placeholders::_2));
    }
    int GetInitiative(const std::string& name) const {
        if(m_template_table.count(name)) {
            return m_template_table.at(name).INIZ_val;
        } else if(m_npc_table.count(name)) {
            return m_npc_table.at(name).INIZ_val;
        } else {
            return m_pc_table.at(name).INIZ_val;
        }
    }
};

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_lwTemplates_doubleClicked(const QModelIndex &index);

    void on_actionRefresh_Templates_triggered();

    void on_pbUp_clicked();

    void on_pbUp_2_clicked();

    void keyPressEvent(QKeyEvent *event);

    void on_pbRemove_clicked();

    void on_actionAdd_PC_triggered();

    void on_lvCharacters_clicked(const QModelIndex &index);

    //menu show
    void ShowMenu(const QPoint& p);

    void change_pf();

    void on_lwDead_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

    void on_lwDead_clicked(const QModelIndex &index);

    void on_actionAdd_NPC_triggered();

    void on_lvCharacters_doubleClicked(const QModelIndex &index);

public slots:
    void lvCharacters_currentChanged(const QModelIndex &cur, const QModelIndex &prev);

private:
    Ui::MainWindow *ui;

    ///my data
    //the available template filename
    std::vector<named_file_type> m_templates;

    //the loaded character
    std::map<std::string, template_npc> m_template_table;
    //the generic npcs
    std::map<std::string, npc> m_npc_table;
    //the char in initiative
    std::map<std::string, character> m_pc_table;

    //the name table
    std::map<std::string, int> m_name_count;

    //the lua state
    lua_State* m_L;

    //my model
    InitiativeListModel m_initiative;
    int m_currentIndex;

    //context menu
    QMenu m_charMenu;

    //insert windows
    npcinsert m_npcinsert_win;
    //PCInsert m_pcinsert_win;

    ///my functions
    //update available templates
    void UpdateTemplates();
    //read NPC from lua script
    template_npc GetNPC(int index);
    //generate unique name
    std::string GenerateUName(const std::string& name);
    //update initiative list
    //add to the initiative list
    //void AddToInitiative(const std::string& name, int initiative, const std::string& ico);
    std::string DescribeNPC(const std::string& uname) const;
    void MenuSetup();
    void ChangePF(int val);
    void DisplayDescription(const std::string& uname);
};

#endif // MAINWINDOW_H
