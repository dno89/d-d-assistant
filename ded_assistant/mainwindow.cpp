#include "mainwindow.h"
#include "ui_mainwindow.h"

///Qt
#include <QFile>
#include <QKeyEvent>
///std
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <strstream>
#include <string>
#include <cstring>
#include <iomanip>
#include <random>
///other forms
#include <pcinsert.h>
#include <npcinsert.h>

///TEMPORARY HARD CONF
static const char* template_folder = "templates/";
static const char* base_script = "base.lua";
static const char* npc_table_script = "npc_table.lua";

//global
static std::default_random_engine eng(time(NULL));
static std::uniform_int_distribution<short> d20(1, 20);

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_initiative(m_npc_table, m_template_table, m_pc_table), m_npcinsert_win(this)
{
    ui->setupUi(this);

    ///LUA SETUP
    //setup the lua state
    m_L = luaL_newstate();
    //open the standard libraries
    luaL_openlibs(m_L);
    //open the base script
    if(luaL_loadfile(m_L, base_script) || lua_pcall(m_L, 0, 0, 0)) {
        std::cerr << "Error while loading/executing the base script: " << base_script << std::endl;
        std::cerr << lua_tostring(m_L, -1) << std::endl;
        exit(1);
    }

    //setup data/model
    ui->lvCharacters->setModel(&m_initiative);

    //grab the keyboard
    //grabKeyboard();

    //current item selection slot
    connect(ui->lvCharacters->selectionModel(), SIGNAL(currentChanged(const QModelIndex&, const QModelIndex&)), this, SLOT(lvCharacters_currentChanged(const QModelIndex&,const QModelIndex&)));


    //charmenu setup
    MenuSetup();
    //custom menu
    ui->lvCharacters->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->lvCharacters, SIGNAL(customContextMenuRequested(const QPoint&)), SLOT(ShowMenu(const QPoint&)));

    m_currentIndex = 0;

    UpdateTemplates();
}

void MainWindow::UpdateTemplates() {
    //list the available templates
    using namespace std;

    //clear old templates
    m_templates.clear();

    //load the available templates
    if(QFile::exists(template_folder)) {
        cout << "folder  '" << template_folder << "' found" << endl;
        QDir tmpl_dir(template_folder);

        QStringList files = tmpl_dir.entryList(QDir::Files);

        for(auto it = files.begin(); it != files.end(); ++it) {
            QString complete_name = *it;

            //remove the final .lua
            if((*it).endsWith(".lua")) {
                (*it).resize((*it).size() - strlen(".lua"));
            }

            m_templates.push_back(make_pair(*it, tmpl_dir.absoluteFilePath(complete_name).toLocal8Bit().data()));
        }

        //test
        cout << "The available templates are:" << endl;
        for(auto nf : m_templates) {
            cout << "\t- Name: " << nf.first.toLocal8Bit().data() << " | FileName: " << nf.second << endl;
        }

    } else {
        cerr << "folder  '" << template_folder << "' NOT found!!" << endl;
    }

    //update the lvTemplates
    ui->lwTemplates->clear();
    for(int ii = 0; ii < m_templates.size(); ++ii) {
        ui->lwTemplates->addItem(m_templates[ii].first);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_lwTemplates_doubleClicked(const QModelIndex &index)
{
    //std::cout << index.row() << std::endl;

    template_npc new_npc;
    try {
        new_npc = GetNPC(index.row());

        new_npc.NAME = GenerateUName(new_npc.NAME);

        std::cout << new_npc.NAME << std::endl;

        //add this npc to the of loaded npc
        m_template_table[new_npc.NAME] = new_npc;

        //add to initiative list
        m_initiative.AddInitiativeEntry(new_npc.NAME);

        //AddToInitiative(new_npc.NAME, new_npc.INIZ_val, new_npc.ico);

    } catch(std::runtime_error& e) {
        std::cerr << "Catched exception: " << e.what() << std::endl;
    }
}

//support function
int GetNumberField(lua_State* l, const char* field) {
    //suppose that the table is at index -1

    //push the field
    lua_getfield(l, -1, field);

    if(!lua_isnumber(l, -1)) {
        throw std::runtime_error("Field " + std::string(field) + " is not a number!!");
    }

    //get the result
    int res = lua_tonumber(l, -1);

    //pop it from the stack
    lua_pop(l, 1);

    return res;
}

int GetNumberIndex(lua_State* l, int index) {
    //suppose that the table is at index -1

    //push the field
    lua_rawgeti(l, -1, index);

    if(!lua_isnumber(l, -1)) {
        throw std::runtime_error("Index is not a number!!");
    }

    //get the result
    int res = lua_tonumber(l, -1);

    //pop it from the stack
    lua_pop(l, 1);

    return res;
}

std::string GetStringField(lua_State* l, const char* field) {
    //suppose that the table is at index -1

    //push the field
    lua_getfield(l, -1, field);

    if(!lua_isstring(l, -1)) {
        throw std::runtime_error("Field " + std::string(field) + " is not a string!!");
    }

    //get the result
    std::string res(lua_tostring(l, -1));

    //pop it from the stack
    lua_pop(l, 1);

    return res;
}

std::string GetStringNumber(lua_State* l, int index) {
    //suppose that the table is at index -1

    //push the field
    lua_rawgeti(l, -1, index);

    if(!lua_isstring(l, -1)) {
        throw std::runtime_error("Index is not a string!!");
    }

    //get the result
    std::string res(lua_tostring(l, -1));

    //pop it from the stack
    lua_pop(l, 1);

    return res;
}

template_npc MainWindow::GetNPC(int index) {
    template_npc new_npc;

    std::string fname = m_templates[index].second;  //the filename of the template

    //execute the template
    if(luaL_loadfile(m_L, fname.c_str()) || lua_pcall(m_L, 0, 0, 0)) {
        std::cerr << "Error while loading/executing template " << fname << std::endl;
        std::cerr << lua_tostring(m_L, -1) << std::endl;

        throw std::runtime_error("Unable to load/execute file");
    }

    //now call the function to build the NPC table
    if(luaL_loadfile(m_L, npc_table_script) || lua_pcall(m_L, 0, 0, 0)) {
        std::cerr << "Error while loading/executing " << npc_table_script << ":" << std::endl;
        std::cerr << lua_tostring(m_L, -1) << std::endl;

        throw std::runtime_error("Error while loading/executing " + std::string(npc_table_script));
    }

    //Ok, now get all the data from the environment
    lua_getglobal(m_L, "NPC");

    //caratteristiche
    new_npc.FOR = GetNumberField(m_L, "FOR");
    new_npc.FOR_mod = GetNumberField(m_L, "FOR_mod");
    new_npc.DES = GetNumberField(m_L, "DES");
    new_npc.DES_mod = GetNumberField(m_L, "DES_mod");
    new_npc.COS = GetNumberField(m_L, "COS");
    new_npc.COS_mod = GetNumberField(m_L, "COS_mod");
    new_npc.INT = GetNumberField(m_L, "INT");
    new_npc.INT_mod = GetNumberField(m_L, "INT_mod");
    new_npc.SAG = GetNumberField(m_L, "SAG");
    new_npc.SAG_mod = GetNumberField(m_L, "SAG_mod");
    new_npc.CAR = GetNumberField(m_L, "CAR");
    new_npc.CAR_mod = GetNumberField(m_L, "CAR_mod");


    //livello
    new_npc.LIV = GetNumberField(m_L, "LIV");

    //BAB
    new_npc.BAB = GetNumberField(m_L, "BAB_val");

    //pf
    new_npc.PF = GetNumberField(m_L, "PF");
    new_npc.remaining_PF = new_npc.PF;

    //VEL
    new_npc.VEL = GetNumberField(m_L, "VEL");

    //DV
    new_npc.DV = GetNumberField(m_L, "DV");

    //armatura
    lua_getfield(m_L, -1, "ARM");
    new_npc.ARM.BONUS = GetNumberField(m_L, "BONUS");
    new_npc.ARM.DES_MAX = GetNumberField(m_L, "DES_MAX");
    new_npc.ARM.NAME = GetStringField(m_L, "NAME");
    new_npc.ARM.PEN = GetNumberField(m_L, "PEN");
    lua_pop(m_L, 1);


    //scudo
    lua_getfield(m_L, -1, "SCU");
    new_npc.SCU.BONUS = GetNumberField(m_L, "BONUS");
    new_npc.SCU.DES_MAX = GetNumberField(m_L, "DES_MAX");
    new_npc.SCU.NAME = GetStringField(m_L, "NAME");
    new_npc.SCU.PEN = GetNumberField(m_L, "PEN");
    lua_pop(m_L, 1);

    //abilita di difesa
    lua_getfield(m_L, -1, "DIF");
    int n_dif = GetNumberIndex(m_L, 0);
    for(int ii = 1; ii <= n_dif; ++ii) {
        std::string dif = GetStringNumber(m_L, ii);
        new_npc.DIF.push_back(dif);
    }
    lua_pop(m_L, 1);

    //abilita di attacco
    lua_getfield(m_L, -1, "ATT");
    int n_att = GetNumberIndex(m_L, 0);
    for(int ii = 1; ii <= n_att; ++ii) {
        std::string att = GetStringNumber(m_L, ii);
        new_npc.ATT.push_back(att);
    }
    lua_pop(m_L, 1);

    //tiri salvezza
    new_npc.TEM = GetNumberField(m_L, "TEM_val");
    new_npc.RIF = GetNumberField(m_L, "RIF_val");
    new_npc.VOL = GetNumberField(m_L, "VOL_val");

    //taglia
    lua_getfield(m_L, -1, "TAG");
    new_npc.TAG = GetStringField(m_L, "NAME");
    lua_pop(m_L, 1);

    //manovre in combattimento
    new_npc.BMC = GetNumberField(m_L, "BMC");
    new_npc.DMC = GetNumberField(m_L, "DMC");

    //talenti
    lua_getfield(m_L, -1, "TAL");
    int n_tal = GetNumberIndex(m_L, 0);
    for(int ii = 1; ii <= n_tal; ++ii) {
        //push the ii-th feat on the stack
        lua_rawgeti(m_L, -1, ii);
        //retrieve the name
        new_npc.TAL.push_back(GetStringField(m_L, "NAME"));
        //pop the feat
        lua_pop(m_L, 1);
    }
    lua_pop(m_L, 1);

    //bonus iniziativa
    new_npc.INIZ = GetNumberField(m_L, "INIZ");

    //valore iniziativa
    new_npc.INIZ_val = GetNumberField(m_L, "INIZ_val");

    //classe armatura
    new_npc.CA = GetNumberField(m_L, "CA");
    new_npc.CA_cont = GetNumberField(m_L, "CA_cont");
    new_npc.CA_sprovv = GetNumberField(m_L, "CA_sprovv");

    //arma da mischia
    lua_getfield(m_L, -1, "A_MIS");
    new_npc.A_MIS.NAME = GetStringField(m_L, "NAME");
    new_npc.A_MIS.TPC = GetNumberField(m_L, "TPC");
    new_npc.A_MIS.DAN = GetNumberField(m_L, "DAN");
    new_npc.A_MIS.DAD = GetStringField(m_L, "DAD");
    new_npc.A_MIS.CRI = GetStringField(m_L, "CRI");
    lua_pop(m_L, 1);

    //arma da distanza
    lua_getfield(m_L, -1, "A_DIS");
    new_npc.A_DIS.NAME = GetStringField(m_L, "NAME");
    new_npc.A_DIS.TPC = GetNumberField(m_L, "TPC");
    new_npc.A_DIS.DAN = GetNumberField(m_L, "DAN");
    new_npc.A_DIS.DAD = GetStringField(m_L, "DAD");
    new_npc.A_DIS.CRI = GetStringField(m_L, "CRI");
    lua_pop(m_L, 1);

    //attacco in mischia
    lua_getfield(m_L, -1, "MIS");
    new_npc.MIS.TPC = GetNumberField(m_L, "TPC");
    new_npc.MIS.DAN = GetStringField(m_L, "DAN");
    lua_pop(m_L, 1);

    //attacco a distanza
    lua_getfield(m_L, -1, "DIS");
    new_npc.DIS.TPC = GetNumberField(m_L, "TPC");
    new_npc.DIS.DAN = GetStringField(m_L, "DAN");
    lua_pop(m_L, 1);

    //the name
    new_npc.NAME = m_templates[index].first.toLocal8Bit().data();

    //get the icon
    new_npc.ico = GetStringField(m_L, "ico");

    //get the money
    lua_getfield(m_L, -1, "MON");
    new_npc.MON.MP = GetNumberField(m_L, "MP");
    new_npc.MON.MO = GetNumberField(m_L, "MO");
    new_npc.MON.MA = GetNumberField(m_L, "MA");
    new_npc.MON.MR = GetNumberField(m_L, "MR");
    lua_pop(m_L, 1);

    //get the items
    lua_getfield(m_L, -1, "OGG");
    int n_ogg = GetNumberIndex(m_L, 0);
    for(int ii = 1; ii <= n_ogg; ++ii) {
        //push the ii-th object on the stack
        lua_rawgeti(m_L, -1, ii);
        //retrieve the name
        new_npc.OGG.push_back(GetStringField(m_L, "NAME"));
        //pop the item
        lua_pop(m_L, 1);
    }
    lua_pop(m_L, 1);

    ///DONE READING
    //pop the NPC table
    lua_pop(m_L, 1);

    //delete the table NPC
    lua_pushnil(m_L);
    lua_setglobal(m_L, "NPC");

    return new_npc;
}

void MainWindow::on_actionRefresh_Templates_triggered()
{
    UpdateTemplates();
}

std::string MainWindow::GenerateUName(const std::string& name) {
    //set the correct name
    if(m_name_count.count(name) == 0) {
        m_name_count[name] = 1;
    } else {
        ++m_name_count[name];
    }
    std::ostrstream uname;
    uname << name << " #" << m_name_count[name] << '\0';

    return uname.str();
}

//void MainWindow::AddToInitiative(const std::string& name, int initiative, const std::string& ico) {
//    new QListWidgetItem(QIcon(ico.c_str()), QString(name.c_str()), ui->lwCharacters);
//}

void MainWindow::on_pbUp_clicked()
{
    int index = ui->lvCharacters->currentIndex().row();
    if(m_initiative.Swap(index, index-1)) {
        ui->lvCharacters->setCurrentIndex(m_initiative.index(index-1, 0));

        //copy the initiative value

    }
}

void MainWindow::on_pbUp_2_clicked()
{
    int index = ui->lvCharacters->currentIndex().row();
    if(m_initiative.Swap(index, index+1)) {
        ui->lvCharacters->setCurrentIndex(m_initiative.index(index+1, 0));
    }
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    //std::cerr << "Keyboard event" << std::endl;
    //std::cerr << event->key() << std::endl;
    if(event->key() == Qt::Key_Return) {
//        std::cerr << "space pressed" << std::endl;
        //int index = ui->lvCharacters->currentIndex().row();

//        std::cerr << "index: " << index << ", rowCount: " << m_initiative.rowCount() << std::endl;

        if(m_currentIndex+1 < m_initiative.rowCount()) {
            ++m_currentIndex;
        } else if(m_currentIndex >= 0 && m_currentIndex == m_initiative.rowCount()-1) {
            m_currentIndex = 0;
        }
        ui->lvCharacters->setCurrentIndex(m_initiative.index(m_currentIndex, 0));
    } else if(event->key() == Qt::Key_Backspace) {
        //std::cerr << "enter pressed" << std::endl;
        ui->lvCharacters->setCurrentIndex(m_initiative.index(m_currentIndex, 0));
    } else if(event->key() == Qt::Key_Plus) {
        ChangePF(1);
    } else if(event->key() == Qt::Key_Minus) {
        ChangePF(-1);
    }
}

void MainWindow::on_pbRemove_clicked()
{
    std::string uname = m_initiative.GetUName(ui->lvCharacters->currentIndex().row());
    int index = ui->lvCharacters->currentIndex().row();
    m_initiative.Remove(index);

    if(index < m_currentIndex) {
        --m_currentIndex;
    }

    //add the removed char to the dead
    if(uname != std::string("")) {
        ui->lwDead->addItem(uname.c_str());
    }
}

//std::string MainWindow::DescribeNPC(const std::string& uname) const {
//    using namespace std;

//    assert(m_template_table.count(uname) || m_npc_table.count(uname));

//    ostrstream out;

//    if(m_template_table.count(uname)) {
//        const template_npc& n = m_template_table.at(uname);

//        const char* title = "===========[ LIVELLO / TAGLIA / DV ]===========";
//        out << title << endl;
//        out << "Tag:\t" << n.TAG << endl;
//        out << "Liv:\t" << n.LIV << endl;
//        out << "DV:\t" << "d" << n.DV << endl;
//        //out << string(strlen(title), '=');
//        title = "===========[ PF / INIZIATIVA / CA ]===========";
//        out << title << endl;
//        out << "PF:\t" << n.remaining_PF << " / " << n.PF << endl;
//        out << "INIZ:\t" << showpos << n.INIZ << noshowpos << " (" << n.INIZ_val << ")" << endl;
//        out << "CA:\t" << n.CA << endl;
//        title = "===========[ CARATTERISTICHE ]===========";
//        out << title << endl;
//        out << noshowpos << "FOR:\t" << n.FOR << "\t| " << showpos << n.FOR_mod << endl;
//        out << noshowpos << "DES:\t" << n.DES << "\t| " << showpos << n.DES_mod << endl;
//        out << noshowpos << "COS:\t" << n.COS << "\t| " << showpos << n.COS_mod << endl;
//        out << noshowpos << "INT:\t" << n.INT << "\t| " << showpos << n.INT_mod << endl;
//        out << noshowpos << "SAG:\t" << n.SAG << "\t| " << showpos << n.SAG_mod << endl;
//        out << noshowpos << "CAR:\t" << n.CAR << "\t| " << showpos << n.CAR_mod << endl << noshowpos;
//        title = "===========[ TIRI SALVEZZA ]===========";
//        out << title << endl;
//        out << "TEM:\t" << showpos << n.TEM << endl;
//        out << "RIF:\t" << n.RIF << endl;
//        out << "VOL:\t" << n.VOL << endl;
//        title = "===========[ COMBATTIMENTO ]===========";
//        out << title << endl;
//        out << "BAB:\t" << n.BAB << endl;
//        out << "BMC:\t" << n.BMC << endl;
//        out << "DMC:\t" << n.DMC << endl;
//        title = "===========[ MISCHIA ]===========";
//        out << title << endl;
//        out << n.A_MIS.NAME << ":\t" << n.MIS.TPC << " (" << n.MIS.DAN << ", " << n.A_MIS.CRI <<")" << endl;
//        title = "===========[ DISTANZA ]===========";
//        out << title << endl;
//        out << n.A_DIS.NAME << ":\t" << n.DIS.TPC << " (" << n.DIS.DAN << ", " << n.A_DIS.CRI << ")" << endl;
//        title = "===========[ ARMATURA / SCUDO ]===========";
//        out << title << endl;
//        out << n.ARM.NAME << ":\t" << showpos << n.ARM.BONUS << " CA, " << n.ARM.PEN << " pen, ";
//        if(n.ARM.DES_MAX > 0) {
//            out << n.ARM.DES_MAX << " DES max";
//        } out << endl;
//        out << n.SCU.NAME << ":\t" << showpos << n.SCU.BONUS << " CA, " << n.SCU.PEN << " pen, ";
//        if(n.SCU.DES_MAX > 0) {
//            out << n.SCU.DES_MAX << " DES max";
//        } out << endl;
//        title = "===========[ TALENTI ]===========";
//        out << title << endl;
//        for(auto t : n.TAL) {
//            out << " > " << t << endl;
//        }
//        title = "===========[ MONETE ]===========";
//        out << title << endl;
//        out << noshowpos << "mp:\t" << n.MON.MP << endl;
//        out << "mo:\t" << n.MON.MO << endl;
//        out << "ma:\t" << n.MON.MA << endl;
//        out << "mr:\t" << n.MON.MR << endl;
//        title = "===========[ OGGETTI ]===========";
//        out << title << endl;
//        for(auto t : n.OGG) {
//            out << " - " << t << endl;
//        }
//        //out << string(strlen(title), '=');

//        out << '\0';
//    } else {
//        const npc& n = m_npc_table.at(uname);

//        const char* title = "===========[ GENERIC NPC ]===========";
//        out << title << endl;
//        out << "PF:\t" << n.remaining_PF << " / " << n.PF << endl;
//        out << "INIZ:\t" << noshowpos << " (" << n.INIZ_val << ")" << endl;
//        out << '\0';
//    }

//    return out.str();
//}

std::string MainWindow::DescribeNPC(const std::string& uname) const {
    using namespace std;

    assert(m_template_table.count(uname) || m_npc_table.count(uname));

    ostrstream out;

    if(m_template_table.count(uname)) {
        const template_npc& n = m_template_table.at(uname);

        const char* title;
        out << "===========[ " << uname << " ]===========" << endl;
        out << "Tag: " << n.TAG << "; Liv: " << n.LIV << "; DV: " << "d" << n.DV << endl;
        out << "INIZ: " << showpos << n.INIZ << noshowpos << " (" << n.INIZ_val << ")" << endl;
        //out << string(strlen(title), '=');
        title = "===========[ DIFESA ]===========";
        out << title << endl;
        out << "CA:\t" << n.CA << " (contatto: " << n.CA_cont << ", sprovvista: " << n.CA_sprovv << ")" << endl;
        out << "PF:\t" << n.remaining_PF << " / " << n.PF << endl;
        out << "TEM: " << showpos << n.TEM << "; RIF: " << n.RIF << "; VOL: " << n.VOL << endl;
        if(!n.DIF.empty()) {
            out << "Abilita' di difesa: " << endl;
            for(auto d : n.DIF) {
                out << "\t> " << d << endl;
            }
        }
        title = "===========[ OFFESA ]===========";
        out << title << endl;
        out << "VEL: " << n.VEL << "m ("<< std::floor(n.VEL/1.5) << " quadretti)" << endl;
        out << "MISCHIA: " << n.A_MIS.NAME << ": " << n.MIS.TPC << " (" << n.MIS.DAN << ", " << n.A_MIS.CRI <<")" << endl;
        out << "DISTANZA: " << n.A_DIS.NAME << ": " << n.DIS.TPC << " (" << n.DIS.DAN << ", " << n.A_DIS.CRI << ")" << endl;

        title = "===========[ STATISTICHE ]===========";
        out << title << endl;
        out << noshowpos << "FOR:" << n.FOR << "; DES: " << n.DES << "; COS: " << n.COS << "; INT: " << n.INT << "; SAG: " << n.SAG << "; CAR: " << n.CAR << ";" << endl;
        out << "BAB: " << n.BAB << "; BMC: " << n.BMC << "; DMC: " << n.DMC << endl;
        title = "===========[ TALENTI ]===========";
        out << title << endl;
        for(auto t : n.TAL) {
            out << " > " << t << endl;
        }
        title = "===========[ ARMATURA / SCUDO ]===========";
        out << title << endl;
        out << n.ARM.NAME << ":\t" << showpos << n.ARM.BONUS << " CA, " << n.ARM.PEN << " pen, ";
        if(n.ARM.DES_MAX > 0) {
            out << n.ARM.DES_MAX << " DES max";
        } out << endl;
        out << n.SCU.NAME << ":\t" << showpos << n.SCU.BONUS << " CA, " << n.SCU.PEN << " pen, ";
        if(n.SCU.DES_MAX > 0) {
            out << n.SCU.DES_MAX << " DES max";
        } out << endl;
        title = "===========[ OGGETTI ]===========";
        out << title << endl;
        out << noshowpos << "mp:\t" << n.MON.MP << endl;
        out << "mo:\t" << n.MON.MO << endl;
        out << "ma:\t" << n.MON.MA << endl;
        out << "mr:\t" << n.MON.MR << endl;
        for(auto t : n.OGG) {
            out << " - " << t << endl;
        }

        out << '\0';
    } else {
        const npc& n = m_npc_table.at(uname);

        const char* title = "===========[ GENERIC NPC ]===========";
        out << title << endl;
        out << "PF:\t" << n.remaining_PF << " / " << n.PF << endl;
        out << "INIZ:\t" << noshowpos << " (" << n.INIZ_val << ")" << endl;
        out << '\0';
    }

    return out.str();
}

void MainWindow::lvCharacters_currentChanged(const QModelIndex& cur, const QModelIndex& prev) {
    int index = cur.row();

    std::string str = m_initiative.GetUName(index);

    DisplayDescription(str);
}

void MainWindow::DisplayDescription(const std::string& uname) {
    if(m_template_table.count(uname) || m_npc_table.count(uname)) {
        ui->tbNPC->setEnabled(true);

        //NPC description
        std::string str = DescribeNPC(uname);

        //set the description
        ui->tbNPC->setText(QString(str.c_str()));
    } else {
        ui->tbNPC->setEnabled(false);
    }
}

void MainWindow::on_actionAdd_PC_triggered()
{
    //std::cerr << "Add triggered" << std::endl;
    PCInsert win(this);
    //win.setModal(true);
    //releaseKeyboard();
    if(win.exec() != QDialog::Rejected) {

        std::string name = win.GetName();
        int init = win.GetInitiative();

        name = GenerateUName(name);
        character new_pc;
        //set the name and the initiative
        new_pc.NAME = name;
        new_pc.INIZ_val = init;

        //add it to the table
        m_pc_table[name] = new_pc;

        //add an initiative entry
        m_initiative.AddInitiativeEntry(name);

        //std::cerr << name << " " << init << std::endl;
    }
    win.hide();
    //win.close();

    //grabKeyboard();
}

void MainWindow::on_lvCharacters_clicked(const QModelIndex &index)
{
    lvCharacters_currentChanged(index, index);
}

void MainWindow::ShowMenu(const QPoint& p) {
    m_charMenu.exec(ui->lvCharacters->mapToGlobal(p));
}

void MainWindow::MenuSetup() {
    QMenu* pf_menu = new QMenu("PF", this);

    for(int ii = 40; ii >= -40; --ii) {
        if(ii == 0) continue;

        QAction* a = pf_menu->addAction(QString((ii > 0?"+%1":"%1")).arg(ii));
        a->setData(ii);
        connect(a, SIGNAL(triggered()), SLOT(change_pf()));
    }

    m_charMenu.addMenu(pf_menu);

}

void MainWindow::change_pf() {
    const QAction * a = qobject_cast<const QAction *>(this->sender());
    int val = a->data().value<int>();
    ChangePF(val);

    //std::cerr << "change_pf called with value: " << val << std::endl;
}

void MainWindow::ChangePF(int val) {
    int index = ui->lvCharacters->currentIndex().row();
    std::string uname = m_initiative.GetUName(index);

    if(m_template_table.count(uname)) {
        m_template_table[uname].remaining_PF += val;

        lvCharacters_currentChanged(m_initiative.index(index), m_initiative.index(index));
    } else if(m_npc_table.count(uname)) {
        m_npc_table[uname].remaining_PF += val;
        lvCharacters_currentChanged(m_initiative.index(index), m_initiative.index(index));
    }

    //std::cerr << "change_pf called with value: " << val << std::endl;
}

void MainWindow::on_lwDead_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    DisplayDescription(current->text().toLocal8Bit().data());
}

void MainWindow::on_lwDead_clicked(const QModelIndex &index)
{
    DisplayDescription(ui->lwDead->item(index.row())->text().toLocal8Bit().data());
}

void MainWindow::on_actionAdd_NPC_triggered() {
    //std::cerr << "Add triggered" << std::endl;
    //releaseKeyboard();
    if(m_npcinsert_win.exec() != QDialog::Rejected) {

        std::string name = m_npcinsert_win.GetUName();
        name = GenerateUName(name);

        int init = m_npcinsert_win.GetIni();
        if(m_npcinsert_win.GetRD20()) {
            init += d20(eng);
        }

        std::string pf = m_npcinsert_win.GetPF();
        int PF;
        pf = "NPC_PF = " + pf;
        if(luaL_loadstring(m_L, pf.c_str()) || lua_pcall(m_L, 0, 0, 0)) {
            std::cerr << "Error while executing pf script" << std::endl;
            std::cerr << lua_tostring(m_L, -1) << std::endl;
            PF = 0;
        } else {
            //get the global value of PF
            lua_getglobal(m_L, "NPC_PF");
            if(lua_isnumber(m_L, -1)) {
                PF = lua_tointeger(m_L, -1);
            } else {
                PF = 0;
            }

            //erase the global value
            lua_pushnil(m_L);
            lua_setglobal(m_L, "NPC_PF");
        }


        npc new_npc;
        //set the name and the initiative
        new_npc.NAME = name;
        new_npc.INIZ_val = init;
        new_npc.PF = PF;
        new_npc.remaining_PF = PF;

        //add it to the table
        m_npc_table[name] = new_npc;

        //add an initiative entry
        m_initiative.AddInitiativeEntry(name);

        //std::cerr << name << " " << init << std::endl;
    }
    m_npcinsert_win.hide();
    //m_npcinsert_win.close();

    //grabKeyboard();
}

void MainWindow::on_lvCharacters_doubleClicked(const QModelIndex &index)
{
    m_currentIndex = index.row();
}
